# **Golang Shell Script Collection for Go Compiler Installation and Management**

## Description
These scripts simplify the process of installing or updating the Golang compiler. Optionally, you can set a custom GOROOT path to make it more flexible for your needs. The scripts will automatically modify the GOROOT directory and relevant shell configuration files, such as .bashrc or .zshrc, ensuring a seamless setup.

Modification of .*rc it will only happen if a path is input and it will insert:
- **export GOROOT=/path/go/goroot**

## Dependencies
Before using the scripts, make sure you have the following dependencies installed on your system:

awk
sed
wget
curl
tar

These dependencies are necessary for the scripts to function properly.

## Usage

To install or update the Golang compiler and optionally set a custom GOROOT path, follow these steps:

1. Clone this repository to your local machine.
```
git clone https://github.com/your-username/Golang-Shell-Scripts.git
cd Golang-Shell-Scripts
```

2. Run the script:
```
./UpGo.sh
```
This will install or update the Golang compiler using default settings.

If you wish to set a custom `GOROOT` path, you can provide it as an argument to the script:
```
./UpGo.sh /path/go/goroot
```
Replace `/path/go/goroot` with the actual path you want to set as your `GOROOT`.

## Examples
For example, to install or update the Go compiler with a custom `GOROOT` path of `/usr/local/go`, use the following command:

```
./UpGo.sh /usr/local/go
```

# License
This project is licensed under the MIT License.