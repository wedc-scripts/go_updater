#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# ==============================================================================
# title             : Semi Automatic Update GoLang
# description       : Script for Install, setup path, permission and update golang
# author            : Walddys Emmanuel Dorrejo Céspedes
# usage             : bash up_install.sh /path/go/goroot
# notes             : Execute this Script will ask sudo password
# dependencies      : wget awk sed curl tar
# ==============================================================================
function sudo_run() {
    if [[ $EUID -ne 0 ]]; then
        echo "This script must be executed with sudo privileges."
        read -sr -N 1 -p "Would you like to execute the script with sudo? (y/N)" VALUE
        case "${VALUE}" in
        y | Y)
            if [ "$(command -v go)" ]; then
                GOROOT=$(go env GOROOT)
                export GOROOT
            fi
            echo -e "\nRelaunching the script with sudo..."
            sleep 2s
            sudo --preserve-env=GOROOT "$0" "$@" # Re-run the script with sudo
            # clear
            exit 1 # Exit the current script
            ;;
        n | N | *)
            echo -e "\nExiting..."
            exit
            ;;
        esac
    fi
}

function version_control() {
    # clear
    printf "Consulting versions availables in the repository"
    ## Get Golang Versions from repository
    readarray -t GO_REPO_VERSION < <(curl -sSL https://go.googlesource.com/go/+refs?format=JSON | grep -Eo "go[0-9]\.[^\"]+" | sort -V)
    ## Delete go_versions RC and Beta from the pool.
    for i in "${!GO_REPO_VERSION[@]}"; do
        if [[ "${GO_REPO_VERSION[i]}" =~ (rc|beta) ]]; then
            unset "GO_REPO_VERSION[i]"
        fi
    done
    GO_REPO_VERSION=("${GO_REPO_VERSION[@]}")

    ## Fix when latest version is repeat one time, in the json it means that is unstable version. (Left in the code if GoDevs went back to the older format again)
    if [[ ${#GO_REPO_VERSION[@]} -ge 2 ]]; then
        last_version=${GO_REPO_VERSION[-1]}
        second_last_version=${GO_REPO_VERSION[-2]}
        last_major_minor=$(echo "${last_version}" | cut -d. -f1-2)
        second_last_major_minor=$(echo "${second_last_version}" | cut -d. -f1-2)
        if [[ "${last_major_minor}" != "${second_last_major_minor}" ]]; then
            unset 'GO_REPO_VERSION[-1]'
        fi
    fi

    ## Get Golang version from repo
    LATEST="${GO_REPO_VERSION[-1]}"
}

function first_time() {
    # clear
    echo "First Time Go Setup"
    echo -e "\tDirectory where is going to be install: ${GOROOT}"
    echo -e "\tLatest Stable Version that will be installed is: ${LATEST}"
    if [[ ! -d /usr/local/go ]]; then
        mkdir "${GOROOT}"
    fi
    echo -e "\nDownloading ${LATEST} and extracting to the install dir ${GOROOT}\n"
    curl -SL --progress-bar https://go.dev/dl/"${LATEST}".linux-amd64.tar.gz | tar -xzf - --strip-components=1 -C "${GOROOT}"
    chown -R "$(id -nu "${SUDO_USER}")" "${GOROOT}"

    if [[ -n "${USERGORROT}" ]]; then
        shell_name=$(basename "$SHELL")
        cat >>~/."${shell_name}"rc <<EOF
    export GOROOT=${GOROOT}
EOF
    fi

    exit 0
}

function update() {
    ## ENV
    COUNTU=0
    COUNTP=0
    COUNTMM=0

    ### Get Current Installed GO Version
    LOCAL=$("${GOROOT}"/bin/go version | grep -Eo "go([0-9]+\.[0-9]+)+")

    ### Get Golang current Major_Minor_Version from local
    if [[ "${LOCAL}" =~ (go[0-9])+\.([0-9]+)\.([0-9]+) ]]; then
	MAJOR_VERSION_LOCAL="${BASH_REMATCH[1]}"
   	MINOR_VERSION_LOCAL="${BASH_REMATCH[2]}"
	PATCH_VERSION_LOCAL="${BASH_REMATCH[3]}"
    	MM_VERSION_LOCAL="${MAJOR_VERSION_LOCAL}.${MINOR_VERSION_LOCAL}"
    fi
    ### Get Golang current Major_Minor_Version from remote
    if [[ "${LATEST}" =~ (go[0-9])+\.([0-9]+)\.([0-9]+) ]]; then
	MAJOR_VERSION_REMOTE="${BASH_REMATCH[1]}"
	MINOR_VERSION_REMOTE="${BASH_REMATCH[2]}"
    	PATCH_VERSION_REMOTE="${BASH_REMATCH[3]}"
	MM_VERSION_REMOTE="${MAJOR_VERSION_REMOTE}.${MINOR_VERSION_REMOTE}"
    fi
    ### Get Golang current Patch_Version of Major_Minor_Version local
    for i in "${!GO_REPO_VERSION[@]}"; do
        if [[ "${GO_REPO_VERSION[i]}" =~ ${MM_VERSION_LOCAL}\.[0-9]+ ]]; then
            PATCH_VERSION_RL="${GO_REPO_VERSION[i]}"
        fi
    done

    printf "%s\n" "Golang Upgrading Protocol Activate" \
        "  Current GoLang Env:" \
        "    - Golang version: ${LOCAL}" \
        "    - Golang installation location: ${GOROOT}"

    ## update
    if (( MAJOR_VERSION_LOCAL -lt MAJOR_VERSION_REMOTE )) || (( MINOR_VERSION_LOCAL -lt MINOR_VERSION_REMOTE )); then
        ((COUNTU++))
        ((COUNTMM++))
    fi
    if [[ "${LOCAL}" != "${PATCH_VERSION_RL}" ]]; then
        ((COUNTU++))
        ((COUNTP++))
    fi

    if [ "${COUNTU}" == 2 ]; then
        while true; do
            echo "Looks like there is a Major/Minor Upgrade ${LATEST}" and a Patch update of the current version "${PATCH_VERSION_RL}"
            read -sr -N 1 -p "What would you like proceed to do? (u/p/c/?)" update
            case ${update} in
            ## Update Major/Minor Version
            u | U)
                while read -r FILE; do
                    rm -rf "${FILE}"
                done -lt <(find "${GOROOT}" -mindepth 1 -maxdepth 1)
                echo -e "\n\nDownloading ${LATEST} and extracting to the install dir ${GOROOT}"
                curl -SL --progress-bar https://go.dev/dl/"${LATEST}".linux-amd64.tar.gz | tar xzf - --strip-components=1 -C "${GOROOT}"
                echo "Updated to latest Major/Minor version: ${LATEST}"
                chown -R "$(id -nu "${SUDO_USER}")" "${GOROOT}"
                exit 0
                ;;
                ## Update Patch Version
            p | P)
                while read -r FILE; do
                    rm -rf "${FILE}"
                done < <(find "${GOROOT}" -mindepth 1 -maxdepth 1)
                echo -e "\n\nDownloading ${PATCH_VERSION_RL} and extracting to the install dir ${GOROOT}"
                curl -SL --progress-bar https://go.dev/dl/"${PATCH_VERSION_RL}".linux-amd64.tar.gz | tar xzf - --strip-components=1 -C "${GOROOT}"
                echo "Updated to latest patch version: ${PATCH_VERSION_RL}"
                chown -R "$(id -nu "${SUDO_USER}")" "${GOROOT}"
                exit 0
                ;;
            c | C)
                echo "Not change made, Exiting update process..."
                sleep 2s
                # clear
                exit 25
                ;;
            ?)
                echo -e "\nu = Update"
                echo "p = Patch"
                echo "c = Cancel"
                echo "? = This help"
                ;;
            *)
                echo "Invalid input..."
                sleep 1.5s
                ;;
            esac
        done
    fi

    if [[ -n "${COUNTMM}" ]]; then
        read -sr -N 1 -p "There is a Major/Minor version upgrade ${LATEST}, want to proceed (y/n)?" update
        case "${update}" in
        y | Y)
            while read -r FILE; do
                rm -rf "${FILE}"
            done < <(find "${GOROOT}" -mindepth 1 -maxdepth 1)
            echo -e "\n\nDownloading ${LATEST} and extracting to the install dir ${GOROOT}"
            curl -SL --progress-bar https://go.dev/dl/"${LATEST}".linux-amd64.tar.gz | tar xzf - --strip-components=1 -C "${GOROOT}"
            echo "Updated to latest patch version: ${LATEST}"
            chown -R "$(id -nu "${SUDO_USER}")" "${GOROOT}"
            exit 0
            ;;
        n | N)
            echo "Not change made, Exiting update process..."
            sleep 2s
            # clear
            exit 25
            ;;
        *)
            echo "Invalid input..."
            sleep 1.5s
            # clear
            ;;
        esac
    fi

    if [[ -n "${COUNTP}" ]]; then
        read -sr -N 1 -p "There is a patch version upgrade, want to proceed (y/n)?" update
        case "${update}" in
        y | Y)
            while read -r FILE; do
                rm -rf "${FILE}"
            done < <(find "${GOROOT}" -mindepth 1 -maxdepth 1)
            echo -e "\n\nDownloading ${PATCH_VERSION_RL} and extracting to the install dir ${GOROOT}"
            curl -SL --progress-bar https://go.dev/dl/"${PATCH_VERSION_RL}".linux-amd64.tar.gz | tar xzf - --strip-components=1 -C "${GOROOT}"
            echo "Updated to latest patch version: ${PATCH_VERSION_RL}"
            chown -R "$(id -nu "${SUDO_USER}")" "${GOROOT}"
            ;;
        n | N)
            echo "Not change made, Exiting update process..."
            sleep 2s
            # clear
            exit 25
            ;;
        *)
            echo "Invalid input..."
            sleep 1.5s
            ;;
        esac
    fi

    echo -e "\nYou are in the latest version of Go"
    sleep 3.5s
}

sudo_run "$@"
version_control

if [ -d "${GOROOT}" ]; then
    update
else
    ## GO Environment
    USERGORROT=$1
    GOROOT=${USERGORROT:-/usr/local/go}
    first_time
fi
